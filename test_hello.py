from hello import hello_world


class TestHelloWorld:

    def test_hello_world(self):
        val = hello_world()
        assert val == 1
