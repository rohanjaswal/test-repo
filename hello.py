from datetime import datetime


def hello_world():
    print("Hello world")
    print(f"UTC time: {datetime.utcnow().strftime('%H:%M:%S')}")
    return 1


if __name__ == "__main__":
    hello_world()
